var module = angular.module("detailNewsModule", ["ngResource"]);

module
.constant("baseURL", "http://localhost:8088/my/news/viewNews/")
.controller("detailNewsCtrl",  function($scope, $http, $routeParams, $rootScope, baseURL) {

  $scope.getDetailNews = function() {

    var url = baseURL + $routeParams.newsId;
    if($rootScope.searchCriteria.tagIds.length > 0 | $rootScope.searchCriteria.authorIds.length > 0 ){
    url = url + "?";

    if($rootScope.searchCriteria.tagIds.length > 0){
    for(var i=0; i<$rootScope.searchCriteria.tagIds.length;i++){
              url = url + "selectedTags=" + $rootScope.searchCriteria.tagIds[i] + "&"
    };
    };

    if($rootScope.searchCriteria.authorIds.length > 0){
    for(var i=0; i<$rootScope.searchCriteria.authorIds.length;i++){
              url = url + "selectedAuthors=" + $rootScope.searchCriteria.authorIds[i] + "&"
    };
    };

    };

    $http.get(url).success(function(response) {
      $scope.detailNews = response[0];
    $scope.previous = response[1];
    $scope.next = response[2];
    })
  };

if(!$rootScope.searchCriteria){
  $rootScope.searchCriteria = {
    authorIds : [],
    tagIds : []
  };
};

$scope.addComment = function(newComment){
  $http.post("http://localhost:8088/my/addComment/" + $routeParams.newsId, newComment).success(function(response) {
  	$scope.detailNews.comments.push(response);
  })
};

$scope.delComment = function(comment) {
  var commentId = comment.commentId;
 $http.delete("http://localhost:8088/my/deleteComment/" + commentId).success(function(response) {
  $scope.detailNews.comments.splice($scope.detailNews.comments.indexOf(comment), 1);
  })

};


});
