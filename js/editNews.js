var moduleEditNews = angular.module("editNews", ["ngResource"]);

moduleEditNews
.controller("editNewsCtrl",  function($scope, $http, $routeParams, $rootScope, $window) {

  $scope.tagsSelected = [];
  $scope.tagsConfig = {displayProp: 'tagName', idProp: 'tagId'};

  $scope.getAllTags = function() {
    $http.get("http://localhost:8088/my/tagsa").success(function(response) {
      $scope.tags = response;
    })
  };



  $scope.authorsSelected = [];
  $scope.authorsConfig = {displayProp: 'authorName', idProp: 'authorId'};

  $scope.getAllAuthors = function() {
    $http.get("http://localhost:8088/my/authors").success(function(response) {
      $scope.authors = response;
    })
  };



$scope.saveEditNewNews = function(){

  if($scope.tagsSelected.length > 0){
    $scope.tags1 = [];
    for(var i=0; i<$scope.tagsSelected.length;i++){
              $scope.tags1.push({tagId : $scope.tagsSelected[i].id});
          }
  };

  if($scope.authorsSelected.length > 0){
    $scope.authors1 = [];
    for(var i=0; i<$scope.authorsSelected.length;i++){
              $scope.authors1.push({authorId : $scope.authorsSelected[i].id});
          }
  };

  $scope.newsData = {
    news : $rootScope.newsForEdit.news,
    tags : $scope.tags1,
    authors : $scope.authors1
  };

  $http.post("http://localhost:8088/my/news/editNews", $scope.newsData).success(function(response) {
     //$window.location.href = '#/news';
  })

};










});
