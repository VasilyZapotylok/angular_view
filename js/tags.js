var module = angular.module("tagModule", ["ngRoute", "ngResource"]);


// module.config(['$httpProvider', function($httpProvider) {
//         $httpProvider.defaults.useXDomain = true;
//         delete $httpProvider.defaults.headers.common['X-Requested-With'];
//     }
// ]);

module.controller("tagCtrl", function($scope, $http, $location) {

	$scope.tags = [];

	$scope.getAll = function() {
		$http.get("http://localhost:8088/my/tagsa").success(function(response) {
			$scope.tags = response;
		})
	};

//$scope.getAll();

$scope.add = function(newTag) {
$http.post("http://localhost:8088/my/tagsa", newTag).success(function(response) {
	$scope.tags.push(response);
	$scope.newTag = {};
})
};

	$scope.del = function(tag) {
		var tagId = tag.tagId;
	 $http.delete("http://localhost:8088/my/tagsa/" + tagId).success(function(response) {
		$scope.tags.splice($scope.tags.indexOf(tag), 1);
		})

 };


	$scope.edit = function(tag) {
    var tagId = tag.tagId;
$http.put("http://localhost:8088/my/tagsa/" + tagId, tag).success(function(response) {
		var tagUpdated = response;
		for (var i = 0; i < $scope.tags.lenght; i++) {
			if ($scope.tags[i].tagId == tagUpdated.tagId) {
				$scope.tags[i] = tagUpdated;
			}
		}
		})
	};

});
