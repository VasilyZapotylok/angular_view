var module = angular.module("mainModule", ["ngRoute", "ngResource", "tagModule", "newsModule", "detailNewsModule", "addNews", "editNews"]);

module.config(function ($routeProvider,$locationProvider, $resourceProvider) {




 //$resourceProvider.defaults.stripTrailingSlashes = false;
// $locationProvider.hashPrefix('!');

	  $routeProvider
        .when('/tags',{
            title: 'Tags Page',
            templateUrl: '/views/tags.html',
            controller: 'tagCtrl'
        })
				.when('/authors',{
						title: 'Authors Page',
						templateUrl: '/views/test.html',
					//	controller: 'authorCtrl'
				})
        .when('/news',{
            title: 'News Page',
            templateUrl: '/views/news.html',
          	controller: 'newsCtrl'
        }).
        when('/news/viewNews/:newsId',{
            title: 'News Page',
            templateUrl: '/views/newsView.html',
            controller: 'detailNewsCtrl'
        }).
				when('/addNews',{
						title: 'Add News Page',
						templateUrl: '/views/addNews.html',
						controller: 'addNewsCtrl'
				}).
				when('/news/editNews/:newsId',{
            title: 'News Page',
            templateUrl: '/views/editNews.html',
            controller: 'editNewsCtrl'
});




  //  $locationProvider.html5Mode(true);


});
module.run(['$rootScope', function($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
}]);









module.controller("mainCtrl", function($scope, $http, $location) {



});
