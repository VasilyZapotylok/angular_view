var module = angular.module("newsModule", ["ngRoute", "ngResource", "angularjs-dropdown-multiselect"]);

module.controller("newsCtrl",  function($scope, $http, $rootScope, $window, $routeParams) {


$scope.tagsSelected = [];
$scope.tags = [];
$scope.tagsConfig = {displayProp: 'tagName', idProp: 'tagId'};

$scope.getAllTags = function() {
  $http.get("http://localhost:8088/my/tagsa").success(function(response) {
    $scope.tags = response;
  })
};

//$scope.getAllTags();

$scope.authorsSelected = [];
$scope.authors = [];
$scope.authorsConfig = {displayProp: 'authorName', idProp: 'authorId'};

$scope.getAllAuthors = function() {
  $http.get("http://localhost:8088/my/authors").success(function(response) {
    $scope.authors = response;
  })
};

//$scope.getAllAuthors();


// criteria start
$scope.makeSearchCriteria = function(){
$scope.tagIds = [];
$scope.authorIds = [];
$rootScope.searchCriteria = {
  authorIds : [],
  tagIds : []
};
  for(var i=0; i<$scope.tagsSelected.length;i++){
            $scope.tagIds[i] = $scope.tagsSelected[i].id;
        }
  for(var i=0; i<$scope.authorsSelected.length;i++){
            $scope.authorIds[i] = $scope.authorsSelected[i].id;
        }
        $rootScope.searchCriteria = {
           authorIds : $scope.authorIds,
           tagIds : $scope.tagIds
       };
       $scope.get(1);
};



$scope.resetSearchCriteria = function(){
  $scope.tagsSelected = [];
  $scope.authorsSelected = [];
  $scope.tagIds = [];
  $scope.authorIds = [];
  $rootScope.searchCriteria = {
    authorIds : [],
    tagIds : []
  };
$scope.get(1);
};
//end


$scope.get = function(page){

var url = "http://localhost:8088/my/news/pages/" + page;
if($rootScope.searchCriteria.tagIds.length > 0 | $rootScope.searchCriteria.authorIds.length > 0 ){
url = url + "?";

if($rootScope.searchCriteria.tagIds.length > 0){
for(var i=0; i<$rootScope.searchCriteria.tagIds.length;i++){
          url = url + "selectedTags=" + $rootScope.searchCriteria.tagIds[i] + "&"
};
};

if($rootScope.searchCriteria.authorIds.length > 0){
for(var i=0; i<$rootScope.searchCriteria.authorIds.length;i++){
          url = url + "selectedAuthors=" + $rootScope.searchCriteria.authorIds[i] + "&"
};
};

};

$http.get(url).success(function(response) {
	$scope.newsList = response[0];
  $scope.pages = response[1];

});

};

$scope.getPages = function(pages) {
    return new Array(pages);
};

$scope.makeSearchCriteria();
//$scope.get(1);


//selector
$scope.selection = [];
$scope.toggleSelection = function toggleSelection(newsId) {
    var idx = $scope.selection.indexOf(newsId);
    // is currently selected
    if (idx > -1) {
      $scope.selection.splice(idx, 1);
    }
    // is newly selected
    else {
      $scope.selection.push(newsId);
    }
  };

//selector

$scope.delListNews = function() {
if($scope.selection.length > 0){
  for(var i=0; i<$scope.selection.length;i++){
    $http.delete("http://localhost:8088/my/deleteNews/" + $scope.selection[i]).success(function(response) {
 $window.location.reload();
     });
        };
};
};


$scope.editNews = function(news){
  $rootScope.newsForEdit = news;
};

});
